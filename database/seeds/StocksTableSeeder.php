<?php

use Illuminate\Database\Seeder;

class StocksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name'          => 'Загородный отдых в семейном Foresta Festival Park отеле со скидкой до 52%!',
                'price'         => 10000,
                'stock'         => 52,
                'economy'       => 5200,
                'date'          => '2018-05-04',
                'date_end'      => '2018-05-18',
                'address'       => 'Московская область, Чеховский район, Любучанский сельский округ, дер. Прохорово, ул. Санаторная, вла...',
                'description'   => 'Скидка на заселение в отель.'
            ],
            [
                'name'          => 'Посещение чудодейственной соляной пещеры «Соль+» со скидкой до 63%',
                'price'         => 3000,
                'stock'         => 20,
                'economy'       => 600,
                'date'          => '2018-05-04',
                'date_end'      => '2018-05-12',
                'address'       => 'Тула, ул. Болдина, д. 107',
                'description'   => 'Скидка на посещение соляной пещеры'
            ],
            [
                'name'          => 'Вкусная и сочная пицца со скидкой 50% от службы доставки "ЭТО ЕДА"!',
                'price'         => 300,
                'stock'         => 50,
                'economy'       => 150,
                'date'          => '2018-05-04',
                'date_end'      => '2018-05-10',
                'address'       => 'Тула, ул. Болдина, д. 107',
                'description'   => 'Скидка на посещение соляной пещеры'
            ]
        ];
        
        foreach ($data as $row) {
            DB::table('stocks')->insert($row);
        }
    }
}
